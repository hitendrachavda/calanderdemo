//
//  ViewController.swift
//  CalanderExample
//
//  Created by Hitendra on 28/12/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import UIKit


class MyDatePicker: UIView {
    
    var changeClosure: ((Date)->())?
    var dismissClosure: (()->())?

    let dPicker: UIDatePicker = {
        let v = UIDatePicker()
        return v
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    func commonInit() -> Void {
//        let blurEffect = UIBlurEffect(style: .dark)
        let blurredEffectView = UIView()

        let pickerHolderView: UIView = {
            let v = UIView()
            v.layer.shadowOpacity = 0.5
            v.layer.shadowOffset = .zero
            v.layer.shadowRadius = 15
            v.backgroundColor = .white
            v.layer.cornerRadius = 8
            v.layer.borderWidth = 0.5
            v.layer.borderColor = UIColor.black.cgColor
            return v
        }()
        
        [blurredEffectView, pickerHolderView, dPicker].forEach { v in
            v.translatesAutoresizingMaskIntoConstraints = false
        }

        addSubview(blurredEffectView)
        pickerHolderView.addSubview(dPicker)
        addSubview(pickerHolderView)
        
        NSLayoutConstraint.activate([
            
            blurredEffectView.topAnchor.constraint(equalTo: topAnchor),
            blurredEffectView.leadingAnchor.constraint(equalTo: leadingAnchor),
            blurredEffectView.trailingAnchor.constraint(equalTo: trailingAnchor),
            blurredEffectView.bottomAnchor.constraint(equalTo: bottomAnchor),

            pickerHolderView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10.0),
            pickerHolderView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10.0),
            pickerHolderView.centerYAnchor.constraint(equalTo: centerYAnchor),

            dPicker.topAnchor.constraint(equalTo: pickerHolderView.topAnchor, constant: 10.0),
            dPicker.leadingAnchor.constraint(equalTo: pickerHolderView.leadingAnchor, constant: 10.0),
            dPicker.trailingAnchor.constraint(equalTo: pickerHolderView.trailingAnchor, constant: -10.0),
            dPicker.bottomAnchor.constraint(equalTo: pickerHolderView.bottomAnchor, constant: -10.0),

        ])
        
        if #available(iOS 14.0, *) {
            dPicker.preferredDatePickerStyle = .inline
        } else {
            // use default
        }
        dPicker.datePickerMode = .date
        dPicker.minimumDate = Date()
        dPicker.maximumDate = Date().Sevendays
        dPicker.addTarget(self, action: #selector(didChangeDate(_:)), for: .valueChanged)
        
        let t = UITapGestureRecognizer(target: self, action: #selector(tapHandler(_:)))
        blurredEffectView.addGestureRecognizer(t)
    }
    
    @objc func tapHandler(_ g: UITapGestureRecognizer) -> Void {
        dismissClosure?()
    }
    
    @objc func didChangeDate(_ sender: UIDatePicker) -> Void {
        changeClosure?(sender.date)
    }
    
}

class ViewController: UIViewController {
    
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var btnSelectDate : UIButton!
    @IBOutlet weak var viewHeader : UIView!
    
    var selectDate : Date?
    
    let myPicker: MyDatePicker = {
            let v = MyDatePicker()
            return v
        }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myPicker.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(myPicker)
        myPicker.backgroundColor = .black.withAlphaComponent(0.1)
        NSLayoutConstraint.activate([
            myPicker.topAnchor.constraint(equalTo: self.view.topAnchor),
            myPicker.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            myPicker.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            myPicker.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
        ])
        myPicker.isHidden = true
        myPicker.dismissClosure = { [weak self] in
            guard let self = self else {
                return
            }
            self.myPicker.isHidden = true
        }
        myPicker.changeClosure = { [weak self] val in
            guard self != nil else {
                return
            }
            print(val)
            // do something with the selected date
        }
        
        self.btnSelectDate.backgroundColor = .white
        self.btnSelectDate.layer.borderWidth = 1.0
        self.btnSelectDate.layer.borderColor = UIColor.gray.cgColor
        self.btnSelectDate.layer.cornerRadius = 10.0
        self.btnSelectDate.dropShadow()
        
        //self.viewHeader.clipsToBounds = true
//        self.viewHeader.backgroundColor = .red
//        self.viewHeader.layer.borderWidth = 1.0
//        self.viewHeader.layer.borderColor = UIColor.gray.cgColor
//        self.viewHeader.layer.cornerRadius = 10.0
        
        self.viewHeader.backgroundColor = .red
        self.viewHeader.layer.cornerRadius = 10.0
        self.viewHeader.layer.borderWidth = 1.0
        self.viewHeader.dropShadow()
        
        self.selectDate = Date()
        let dateFormatter = DateFormatter.init()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd/MM/yyyy"
        self.lblDate.text = dateFormatter.string(from: Date())
    }
    
    @objc func tap(_ sender: Any) {
        myPicker.isHidden = false
    }
    static func getViewController(_ identifier : String) -> UIViewController{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: identifier)
    }
    @IBAction func openDate(_ sender : UIButton){
        
        self.tap(sender)
        return
        
        let dateFormatter = DateFormatter.init()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let calanderVC = ViewController.getViewController(String(describing: CalanderVC.self)) as! CalanderVC
        if(selectDate != nil){
            calanderVC.selectedDate = selectDate as NSDate?
        }
        calanderVC.selectDate = { [weak self] (date) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.selectDate = date as Date
            strongSelf.lblDate.text = dateFormatter.string(from: date as Date)
        }
        calanderVC.closeViewController = { [weak self] (viewCon) in
            guard self != nil else {
                return
            }
            viewCon.dismiss(animated: false, completion: nil)
        }
        calanderVC.presentViewControllerForViewController(self)
        calanderVC.borderShow(2.0, 10.0)
    }
}

extension Date {
    var Sevendays: Date {
        return Calendar.current.date(byAdding: .day, value: 7, to: self)!
    }

    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: self)!
    }
}
